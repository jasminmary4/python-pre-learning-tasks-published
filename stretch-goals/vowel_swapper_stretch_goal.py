def vowel_swapper(string):
    # ==============
    string = string.lower()
    afind = "a"
    efind = "e"
    ifind = "i"
    ofind = "o"
    ufind = "u"
    if string.count(afind) > 1:
        a = string.index("a", string.index("a") + 1)
        b = string[:a] + "4" + string[a + 1:]
        string = b

    if string.count(efind) > 1:
        c = string.index("e", string.index("e") + 1)
        d = string[:c] + "3" + string[c + 1:]
        string = d

    if string.count(ifind) > 1:
        e = string.index("i", string.index("i") + 1)
        z = string[:e] + "!" + string[e + 1:]
        string = z

    if string.count(ofind) > 1:
        g = string.index("o", string.index("o") + 1)
        h = string[:g] + "000" + string[g + 1:]
        string = h

    if string.count(ufind) > 1:
        i = string.index("u", string.index("u") + 1)
        j = string[:i] + "|_|" + string[i + 1:]
        string = j

    return string


print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
