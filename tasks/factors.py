def factors(number):
    # ==============
    # Your code here
    a = []
    for x in range(2, number):
        if (number % x) == 0:
            a.append(x)
    return a
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
